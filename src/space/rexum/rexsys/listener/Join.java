package space.rexum.rexsys.listener;

import space.rexum.rexsys.RexSYS;
import space.rexum.rexsys.manager.FileManager;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class Join implements Listener {
	
	private FileManager getFileManager() {
		return this.fileManager;
		} private FileManager fileManager = RexSYS.getInstance().getFileManager();

    @EventHandler
    public void onJoin(final PlayerJoinEvent event){
        event.setJoinMessage("�a+ �8[�e" + event.getPlayer().getName() + "�8]");
        Player player = event.getPlayer();
        if (getFileManager().isTeleportOnJoin()) {
        Location spawn = getFileManager().getSpawnLocation();
        player.teleport(spawn);
        } 
    }

    @EventHandler
    public void onQuit(final PlayerQuitEvent event){
        event.setQuitMessage(null);
    }

}