package space.rexum.rexsys.listener;

import space.rexum.rexsys.RexSYS;
import space.rexum.rexsys.manager.FileManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

public class Motd
/*    */   implements Listener
/*    */ {
/*    */   private FileManager getFileManager() {
/* 13 */     return this.fileManager;
/* 14 */   } private FileManager fileManager = RexSYS.getInstance().getFileManager();
/*    */   
/*    */   @EventHandler
/*    */   public void onPing(ServerListPingEvent event) {
/* 18 */     if (getFileManager().isUseMotD())
/* 19 */       event.setMotd(getFileManager().getMotD_L1() + "\n" + getFileManager().getMotD_L2()); 
/*    */   }
/*    */ }