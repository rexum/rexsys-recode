package space.rexum.rexsys.manager;

import space.rexum.rexsys.RexSYS;
import org.bukkit.Bukkit;
import org.bukkit.Location;
/*    */ 
/*    */ public class FileManager {
/*    */   private final FileWriter fileWriter;
/*    */   
/*    */   public FileWriter getFileWriter() {
/* 11 */     return this.fileWriter;
/*    */   }
/*    */   
/* 14 */   public Location getSpawnLocation() { return this.spawnLocation; } public void setSpawnLocation(Location spawnLocation) { this.spawnLocation = spawnLocation; }
/* 15 */    private Location spawnLocation = Bukkit.getWorld("world").getSpawnLocation(); private boolean teleportOnJoin = true;
/*    */   public boolean isTeleportOnJoin() {
/* 17 */     return this.teleportOnJoin;
/*    */   }
/*    */   
/*    */    private boolean useMotD = true;
/*    */   public boolean isUseMotD() {
/* 35 */     return this.useMotD;
/*    */   }
/*    */   
/* 38 */   private String motD_L1 = "&6•● &eDeinServer&7.net  &f-  &bRexSYS &8[&d1.16.5&8]"; public String getMotD_L1() { return this.motD_L1; }
/*    */ 
/*    */   
/* 41 */   private String motD_L2 = "  &8➜ &cmade by rexum with &4❤"; public String getMotD_L2() { return this.motD_L2; }
/*    */ 
/*    */   
/*    */   public FileManager(RexSYS miniTools) {
/* 45 */     this.fileWriter = new FileWriter(miniTools.getDataFolder().getPath(), "config.yml");
/*    */     
/* 47 */     loadFile();
/* 48 */     readFile();
/*    */   }
/*    */   
/*    */   private void loadFile() {
/* 52 */     if (!this.fileWriter.valueExist("spawnLocation")) this.fileWriter.setLocation(this.spawnLocation, "spawnLocation"); 
/* 53 */     this.fileWriter.setDefaultValue("teleportOnJoin", Boolean.valueOf(this.teleportOnJoin));
/* 59 */     this.fileWriter.setDefaultValue("useMotD", Boolean.valueOf(this.useMotD));
/* 60 */     this.fileWriter.setDefaultValue("MotD.Line1", this.motD_L1);
/* 61 */     this.fileWriter.setDefaultValue("MotD.Line2", this.motD_L2);
/*    */   }
/*    */ 
/*    */ 
/*    */   
/*    */   private void readFile() {
/* 67 */     this.spawnLocation = this.fileWriter.getLocation("spawnLocation");
/* 68 */     this.teleportOnJoin = this.fileWriter.getBoolean("teleportOnJoin");
/* 74 */     this.useMotD = this.fileWriter.getBoolean("useMotD");
/* 75 */     this.motD_L1 = this.fileWriter.getFormatString("MotD.Line1");
/* 76 */     this.motD_L2 = this.fileWriter.getFormatString("MotD.Line2");
/*    */   }
/*    */ 
/*    */   
/*    */   public void reload() {
/* 81 */     readFile();
/*    */   }
/*    */ }