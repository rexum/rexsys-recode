package space.rexum.rexsys.manager;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
 

public class FileWriter {
	private File f;
	private YamlConfiguration c;
	
	public FileWriter(String FilePath, String FileName) {
		this.f = new File(FilePath, FileName);
		this.c = YamlConfiguration.loadConfiguration(this.f);
		} 
	public boolean exist() {
		return this.f.exists();
		}
	
	public FileWriter setValue(String ValuePath, Object Value) {
		this.c.set(ValuePath, Value);
		save();
		return this;
		}
	
	public FileWriter setDefaultValue(String ValuePath, Object Value) {
		if (!valueExist(ValuePath)) {
			this.c.set(ValuePath, Value);
			save();
			} 
		return this;
		}
	
	public Object getObject(String ValuePath) {
		return this.c.get(ValuePath);
		}
	
	public boolean valueExist(String valuePath) {
		return (getObject(valuePath) != null);
		}
	
	public FileWriter save() {
		try {
			this.c.save(this.f);
			} catch (IOException e) {
				e.printStackTrace();
				} 
		return this;
		}
	
	public boolean getBoolean(String ValuePath) {
		return this.c.getBoolean(ValuePath);
		}  

	public String getString(String ValuePath) {
		return this.c.getString(ValuePath);
		}

	public Integer getInt(String ValuePath) {
		return Integer.valueOf(this.c.getInt(ValuePath));
		}
	
	public List<String> getStringList(String ValuePath) {
		return this.c.getStringList(ValuePath);
		}
	
	public List<Integer> getIntList(String ValuePath) {
		return this.c.getIntegerList(ValuePath);
		}
	
	public double getDouble(String ValuePath) {
		return this.c.getDouble(ValuePath);
		}
	
	public Float getFloat(String ValuePath) {
		return Float.valueOf((float)this.c.getLong(ValuePath));
		}
	
    public Long getLong(String ValuePath) {
        return this.c.getLong(ValuePath);
    }
	
	public String getFormatString(String ValuePath) {
		return ((String)Objects.<String>requireNonNull(this.c.getString(ValuePath))).replaceAll("&", "�");
		}
	
	public void setLocation(Location loc, String path) {
		this.c.set(path + ".x", Double.valueOf(loc.getX()));
		this.c.set(path + ".y", Double.valueOf(loc.getY()));
		this.c.set(path + ".z", Double.valueOf(loc.getZ()));
		this.c.set(path + ".yaw", Float.valueOf(loc.getYaw()));
		this.c.set(path + ".pitch", Float.valueOf(loc.getPitch()));
		this.c.set(path + ".world", loc.getWorld().getName());
		save();
		}

	public Location getLocation(String path) {
		Location loc;
		double x = this.c.getDouble(path + ".x");
		double y = this.c.getDouble(path + ".y");
		double z = this.c.getDouble(path + ".z");
		float yaw = (float)this.c.getDouble(path + ".yaw");
		float pitch = (float)this.c.getDouble(path + ".pitch");
		
		try {
			World world = Bukkit.getWorld(getString(path + ".world"));
			loc = new Location(world, x, y, z, yaw, pitch);
			} catch (Exception unused) {
				loc = null;
				} 
		return loc;
		}
	}
