package space.rexum.rexsys.manager;
import space.rexum.rexsys.RexSYS;

public class MessageGetter {

    private FileWriter fileWriter;

    public MessageGetter(){
        fileWriter = new FileWriter(RexSYS.getInstance().getDataFolder().getPath(), "messages.yml");

        fileWriter.setDefaultValue("TeamSpeak", "%prefix%Unser TeamSpeak: &bts.rexum.space");
        fileWriter.setDefaultValue("Discord", "%prefix%Unser Discord Server: &bhttps://discord.gg/deinserver");
        fileWriter.setDefaultValue("Twitter", "%prefix%&9Folge unserem Twitter Account: &bhttps://twitter.com/");
        fileWriter.setDefaultValue("Instagram", "%prefix%Folge unserem Instagram Account: &bhttps://instagram.com/");
        fileWriter.setDefaultValue("Forum", "%prefix%&cEin Forum besitzen wir leider noch nicht. &e:c");
        fileWriter.setDefaultValue("Shop", "%prefix%&eHier kommst du zu unserem Shop: &bshop.DeinServer.net");
    }

    public String getMessaage(final String path){
        if(!fileWriter.valueExist(path)){
            return "�4Fehler: �Der Wert konnte in der messages.yml nicht gefunden werden.";
        }
        return fileWriter.getFormatString(path).replace("%prefix%", RexSYS.prefix);
    }

    public void reload(){
        fileWriter = new FileWriter(RexSYS.getInstance().getDataFolder().getPath(), "messages.yml");
    }

}
