package space.rexum.rexsys.command;

import space.rexum.rexsys.RexSYS;
import space.rexum.rexsys.manager.Data;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class setSpawn
implements CommandExecutor
{
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(space.rexum.rexsys.RexSYS.prefix + "Du musst ein Spieler sein.");
			return false;
			} 
		
		Player player = (Player)sender;
		
		if (player.hasPermission("rexsys.setspawn")) {
			RexSYS.getInstance().getFileManager().getFileWriter().setLocation(player.getLocation(), "spawnLocation");
			RexSYS.getInstance().getFileManager().setSpawnLocation(player.getLocation());
			player.sendMessage(space.rexum.rexsys.RexSYS.prefix + "�aDu hast den Spawn gesetzt.");
			} else {
				player.sendMessage(Data.getNoPerm());
				} 
		
		return false;
		}
	}