package space.rexum.rexsys.command;

import space.rexum.rexsys.manager.Data;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class GiveAll implements CommandExecutor {
  @SuppressWarnings("deprecation")
public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage("Du musst ein Spieler sein.");
      return false;
    } 
    Player player = (Player)sender;
    if (player.hasPermission("rexsys.giveall")) {
      if (args.length == 0) {
		ItemStack item = player.getItemInHand();
        player.setItemInHand(new ItemStack(Material.AIR));
        if (item.getType() != Material.AIR) {
          for (Player all : Bukkit.getOnlinePlayers()) {
            all.sendMessage("");
            if (item.getItemMeta().getDisplayName() != null) {
              all.sendMessage(space.rexum.rexsys.RexSYS.prefix + "haben das Item + item.getItemMeta().getDisplayName() + " );
            } else {
              all.sendMessage(space.rexum.rexsys.RexSYS.prefix + "haben das Item + item.getType() + " );
            } 
            all.sendMessage("");
            all.getInventory().addItem(new ItemStack[] { item });
            all.playSound(all.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 3.0F, 5.0F);
          } 
        } else {
          player.sendMessage(space.rexum.rexsys.RexSYS.prefix + "hast kein Item in der Hand.");
        } 
      } else {
        player.sendMessage(space.rexum.rexsys.RexSYS.prefix + "Usage: /giveall");
      } 
    } else {
      player.sendMessage(Data.getNoPerm());
    } 
    return false;
  }
}
