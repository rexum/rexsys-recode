package space.rexum.rexsys.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TeamChat implements CommandExecutor {
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(!(sender instanceof Player)){
            sender.sendMessage(space.rexum.rexsys.RexSYS.prefix + "Du musst ein Spieler sein.");
            return false;
        }

        final Player player = ((Player) sender);

        if(player.hasPermission("rexsys.teamchat.use")){

            if(args.length >= 1){
                StringBuilder stringBuilder = new StringBuilder();

                for(String arg : args){
                    stringBuilder.append(arg).append(" ");
                }

                sendTeamChat(player.getName(), stringBuilder.toString(), "rexsys.teamchat.use");

            }else{
                player.sendMessage(space.rexum.rexsys.RexSYS.prefix + "Usage: /tc <Nachricht>");
            }

        }else{
            player.sendMessage(space.rexum.rexsys.RexSYS.prefix + "�cDazu hast du keinen Zugriff.");
        }

        return false;
    }

    private void sendTeamChat(final String fromName, final String message, final String perm){
        Bukkit.getOnlinePlayers().forEach(players -> {
            if(players.hasPermission(perm)){
                players.sendMessage(space.rexum.rexsys.RexSYS.prefix + "�8[�b" + fromName + "�8] �f" + message.replace('&', '�'));
            }
        });
    }

}