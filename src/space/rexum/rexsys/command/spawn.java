package space.rexum.rexsys.command;

import space.rexum.rexsys.RexSYS;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/*    */ public class spawn
/*    */   implements CommandExecutor
/*    */ {
/*    */   public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
/* 15 */     if (!(sender instanceof Player)) {
/* 16 */       sender.sendMessage(space.rexum.rexsys.RexSYS.prefix + "Du musst ein Spieler sein.");
/* 17 */       return false;
/*    */     } 
/*    */     
/* 20 */     Player player = (Player)sender;
/* 21 */     Location spawn = RexSYS.getInstance().getFileManager().getSpawnLocation();
/* 22 */     player.teleport(spawn);
/* 23 */     player.sendMessage(space.rexum.rexsys.RexSYS.prefix + "�aDu wurdest zum Spawn teleportiert.");
/*    */     
/* 25 */     return false;
/*    */   }
/*    */ }