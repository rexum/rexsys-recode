package space.rexum.rexsys.command;

import space.rexum.rexsys.manager.Data;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Rename implements CommandExecutor {
  @SuppressWarnings("deprecation")
public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage("Du musst ein Spieler sein.");
      return false;
    } 
    Player player = (Player)sender;
    if (player.hasPermission("rexsys.rename")) {
      if (args.length >= 1) {
        ItemStack item = player.getItemInHand();
        if (item.getType() != Material.AIR) {
          StringBuilder sb = new StringBuilder();
          int i = 0;
          while (i < args.length) {
            if (i < 1) {
              sb.append(String.valueOf(args[i]));
            } else {
              sb.append(" ").append(String.valueOf(args[i]));
            } 
            i++;
          } 
          String message = sb.toString();
          message = message.replaceAll("&", "�");
          ItemMeta im = item.getItemMeta();
          assert im != null;
          im.setDisplayName(message);
          item.setItemMeta(im);
          player.setItemInHand(item);
          player.sendMessage(space.rexum.rexsys.RexSYS.prefix + "Du hast das Item umbenannt: " + message);
          player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0F, 1.0F);
        } else {
          player.sendMessage(space.rexum.rexsys.RexSYS.prefix + "hast kein Item in der Hand.");
        } 
      } else {
        player.sendMessage(space.rexum.rexsys.RexSYS.prefix + "Usage: /rename <Name>");
      } 
    } else {
      player.sendMessage(Data.getNoPerm());
    } 
    return false;
  }
}