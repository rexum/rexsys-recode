package space.rexum.rexsys;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import space.rexum.rexsys.manager.FileManager;
import space.rexum.rexsys.manager.MessageGetter;

import org.bukkit.plugin.PluginManager;


public class RexSYS extends JavaPlugin{
	private static RexSYS instance;
	public final static String prefix = "�8[�bSystem�8] �7";
    private static MessageGetter messageGetter;
	
	
	public static RexSYS getInstance() {
		return instance;
		} private FileManager fileManager;
		public FileManager getFileManager() {
			return this.fileManager;
			}
	
	@Override
	public void onEnable() {
		instance = this;  
		this.fileManager = new FileManager(this);
        messageGetter = new MessageGetter();
		loadCommands();
		loadListener(Bukkit.getPluginManager());
		log("�aPlugin geladen.");
	}
	
	@Override
	public void onDisable() {
		log("�cPlugin entladen.");
	}
	
    private void loadCommands(){
        getCommand("kick").setExecutor(new space.rexum.rexsys.command.Kick());
        getCommand("clearchat").setExecutor(new space.rexum.rexsys.command.ClearChat());
        getCommand("rex").setExecutor(new space.rexum.rexsys.command.RexCommand());
        getCommand("fly").setExecutor(new space.rexum.rexsys.command.Fly());
        getCommand("top").setExecutor(new space.rexum.rexsys.command.Top());
        getCommand("tp").setExecutor(new space.rexum.rexsys.command.Tp());
        getCommand("heal").setExecutor(new space.rexum.rexsys.command.Heal());
        getCommand("feed").setExecutor(new space.rexum.rexsys.command.Feed());
        getCommand("gm").setExecutor(new space.rexum.rexsys.command.Gamemode());
        getCommand("msg").setExecutor(new space.rexum.rexsys.command.Msg());
        getCommand("rtp").setExecutor(new space.rexum.rexsys.command.RandomTP());
        getCommand("setspawn").setExecutor(new space.rexum.rexsys.command.setSpawn());
        getCommand("spawn").setExecutor(new space.rexum.rexsys.command.spawn());
        getCommand("giveall").setExecutor(new space.rexum.rexsys.command.GiveAll());
        getCommand("rename").setExecutor(new space.rexum.rexsys.command.Rename());
        getCommand("discord").setExecutor(new space.rexum.rexsys.command.Discord());
        getCommand("forum").setExecutor(new space.rexum.rexsys.command.Forum());
        getCommand("ts").setExecutor(new space.rexum.rexsys.command.TeamSpeak());
        getCommand("twitter").setExecutor(new space.rexum.rexsys.command.Twitter());
        getCommand("shop").setExecutor(new space.rexum.rexsys.command.Shop());
        getCommand("instagram").setExecutor(new space.rexum.rexsys.command.Instagram());
        getCommand("tc").setExecutor(new space.rexum.rexsys.command.TeamChat());
    }
    
    
    private void loadListener(final PluginManager pluginManager){
        pluginManager.registerEvents(new space.rexum.rexsys.listener.Join(), this);
        pluginManager.registerEvents(new space.rexum.rexsys.listener.Chat(), this);
        pluginManager.registerEvents(new space.rexum.rexsys.listener.Motd(), this);

    }
	
    private void log(final String message){
    	Bukkit.getConsoleSender().sendMessage(prefix + message);
    }

	public static MessageGetter getMessageGetter() {
		return messageGetter;
	}

	public static void setMessageGetter(MessageGetter messageGetter) {
		RexSYS.messageGetter = messageGetter;
	}

}
