# RexSYS-recode

Download: https://git.io/JtW9b

SpigotMC: https://www.spigotmc.org/resources/rexsys.88342/

DevBukkit: https://dev.bukkit.org/projects/rexsys

BlackSpigot: https://www.blackspigot.com/downloads/rexsys.19908/

Spigot: https://sourceforge.net/projects/rexum/files/spigot/

Perms: https://github.com/rexjohannes/RexSYS-recode/wiki/Perms

Java: https://adoptopenjdk.net/?variant=openjdk11&jvmVariant=hotspot

<a href="https://zap-hosting.com/rexsys"><img src="https://zap-hosting.com/interface/download/images.php?type=affiliate&id=65869" alt="ZAP-Hosting Gameserver and Webhosting"></a>
